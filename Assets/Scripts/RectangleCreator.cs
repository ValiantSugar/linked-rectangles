﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RectangleCreator : MonoBehaviour, IPointerClickHandler
{
	[SerializeField]
	GameObject prefabRectangle;
	Vector2 sizeRect;
    
	void Awake()
	{
		sizeRect = prefabRectangle.GetComponent<BoxCollider2D>().size;
	}
	
	/*
	Так же, прямоугольники можно создавать с помощью Input.GetMouseButton и Rayсast'а, 
	однако я считаю использование интерфейсов IPointerHandler более оптимизированным вариантом
	так как вызывается только 1 раз и работает с Touch'ами.
	Так же можно было бы использовать OnMouseDown(), то он устарел и не работает с Touch'ами
	*/
	public void OnPointerClick(PointerEventData pointerEventData)
    {
		//Можно прикрутить пулл объектов, но не вижу в нем смысла в такой простой задаче
		Vector3 pos = Camera.main.ScreenToWorldPoint(pointerEventData.position) - new Vector3(0f, 0f, -10f);
		
		//Берем размер соллайдера на префабе прямоугольника и поделив его пополам 
		//находим две точки для проверки попадания в них коллайдера прямоугольника
		if (Physics2D.OverlapArea(new Vector2(sizeRect.x / -2 + pos.x, sizeRect.y / -2 + pos.y),
									new Vector2(sizeRect.x / 2 + pos.x, sizeRect.y / 2 + pos.y), 1<<8))
		{
			Debug.Log("Невозможно построить прямоугольник");
		}
		else
		{
			Instantiate(prefabRectangle, pos, Quaternion.identity);
		}
		
		//Удаление возможной связи
		LinkManager.o.ClearCreator();
	}
}
