﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Объект связи
public class Link 
{
	public Rectangle startRect, endRect;
	public LineRenderer line;
	
	public Link (Rectangle start, Rectangle end, LineRenderer newLine)
	{
		startRect = start;
		endRect = end;
		line = newLine;
	}
}

public class LinkManager : MonoBehaviour
{
    public static LinkManager o;
	
	[SerializeField]
	GameObject prefabLine;
	
	Rectangle tempRect;
	
	LineRenderer currentLine;
	
	List<Link> allLinks = new List<Link>();
	
	void Awake()
	{
		if (o != null)
		{
			Destroy(gameObject);
			return;
		}
		o = this;
	}
	
	void Update()
	{
		//Отрисовывает линию во время перемещения мыши
		if (currentLine != null)
		{
			currentLine.SetPosition(0, tempRect.transform.position);
			currentLine.SetPosition(1, Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
	}
	
	public void CreateLink(Rectangle rect)
	{
		//Выбор начальной точки связи
		if (tempRect == null)
		{
			tempRect = rect;
			currentLine = Instantiate(prefabLine, transform.position, Quaternion.identity).GetComponent<LineRenderer>();
			return;
		}
		
		//Выбор конечной точки связи
		if (tempRect != rect)
		{
			//Проверка на наличие уже существующих таких же связей
			for (int i = 0; i < tempRect.myLinks.Count; i++)
			{
				//Если такая связь существует - она удаляется
				if (rect.myLinks.Remove(tempRect.myLinks[i]))
				{
					DeleteLink(tempRect.myLinks[i]);
					ClearCreator();
					return;
				}
			}
			
			//Создание объекта связи
			LineRenderer lineTemp = Instantiate(prefabLine, transform.position, Quaternion.identity).GetComponent<LineRenderer>();
			lineTemp.transform.SetParent(transform);
			lineTemp.SetPosition(0, tempRect.transform.position);
			lineTemp.SetPosition(1, rect.transform.position);
			allLinks.Add(new Link(tempRect, rect, lineTemp));
			//Присвоение ссылки объекта связи прямоугольникам
			tempRect.myLinks.Add(allLinks[allLinks.Count - 1]);
			rect.myLinks.Add(allLinks[allLinks.Count - 1]);
		}
		
		ClearCreator();
	}
	
	//Отрисовка связей в реальном времени при перемещении прямоугольника
	public void UpdateLines(List<Link> links)
	{
		for (int i = 0; i < links.Count; i++)
		{
			Link tempLink = links[i];
			tempLink.line.SetPosition(0, tempLink.startRect.transform.position);
			tempLink.line.SetPosition(1, tempLink.endRect.transform.position);
		}
	}
	
	//Удаление связи
	public void DeleteLink(Link link)
	{
		link.startRect.myLinks.Remove(link);
		link.endRect.myLinks.Remove(link);
		Destroy(link.line.gameObject);
		allLinks.Remove(link);
	}
	
	//Если связь не создается по причине отсутствия конечного связующего
	public void ClearCreator()
	{
		tempRect = null;
		if (currentLine != null)
		{
			Destroy(currentLine.gameObject);
		}
		
	}
}
