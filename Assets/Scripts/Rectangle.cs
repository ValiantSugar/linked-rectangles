﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Rectangle : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerExitHandler
{
    Rigidbody2D myRB;
	bool taken;
	Vector3 gap;
	int clickCount;
	float clickTimer;
	
	public List<Link> myLinks = new List<Link>();
	
	void Awake()
	{
		myRB = GetComponent<Rigidbody2D>();
		GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
	}
	
	void Update()
	{
		if (clickTimer > 0f)
		{
			clickTimer -= Time.deltaTime;
			if (clickTimer < 0f)
			{
				clickCount = 0;
			}
		}
	}
	
	void FixedUpdate()
	{
		if (taken)
		{
			myRB.MovePosition(Camera.main.ScreenToWorldPoint(Input.mousePosition) + gap);
			LinkManager.o.UpdateLines(myLinks);
		}
	}
	
	public void OnPointerDown(PointerEventData pointerEventData)
    {
		taken = true;
		myRB.velocity = new Vector2(0, 0);
        myRB.bodyType = RigidbodyType2D.Dynamic;
		/*
		Разрыв между позицией курсора и пивота прямоугольника, что бы при подборе
		прямоугольник не "телепортировался" на курсор
		*/
		gap = transform.position - (Camera.main.ScreenToWorldPoint(pointerEventData.position) - new Vector3(0f, 0f, -10f));
		
		clickTimer = 0.2f;
	}
	
	public void OnPointerClick(PointerEventData pointerEventData)
    {
		taken = false;
		myRB.bodyType = RigidbodyType2D.Kinematic;
		//Счетчик для двойного клика
		clickCount++;
		if (clickCount == 2)
		{
			Destroy(gameObject);
			return;
		}
		
		if (clickTimer > 0f) 
		{
			//Создание связи
			LinkManager.o.CreateLink(this);
		}
		
		clickTimer = 0.5f;
	}
	
	public void OnPointerExit(PointerEventData pointerEventData)
    {
		taken = false;
		myRB.bodyType = RigidbodyType2D.Kinematic;
		LinkManager.o.UpdateLines(myLinks);
	}
	
	//При уничтожении объекта все связи удаляются
	void OnDestroy()
	{
		while (myLinks.Count > 0)
		{
			LinkManager.o.DeleteLink(myLinks[0]);
		}
		LinkManager.o.ClearCreator();
	}
}
